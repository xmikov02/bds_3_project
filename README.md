# Library database
This is the third semestral project for subject “Bezpečnost databázových systémů“ assigned by Brno University of Technology. This project deals with library database based on PostgreSQL database from previous assignment. The goal of the application is to implement a Desktop application in Java compilable and runnable from command-line, which enables authenticated db system users to have an access to database, use operations to change the database (such as CRUD operations) or to try an injection simulation.

## How to build and run the program
STEP 1:    
git clone https://gitlab.com/xmikov02/bds_3_project

STEP 2:    
`$ cd bds-library-main`

STEP 3:    
 `$ mvn clean install`

STEP 4:    
`$ cd target`

STEP 5:    
`$ java -jar bds-library-1.0.0.jar`

## How to log into the app
You can choose any username stored in the database. Password is the same for all users.
For example try username  libby.dougherty@gmail.com and password: ilovebds.  
