echo off
echo 'Generate backup file name'

set BACKUP_FOLDER=C:\backupdb
set BACKUP_FILE=%date%.custom.backup

echo 'Backup path: %BACKUP_FILE%'
echo 'Creating a backup ...'

set PGPASSWORD=ilovebds
"C:\Program Files\PostgreSQL\14\bin\pg_dump.exe" --username="db_user" -d pls_be_last_db --format=custom -f "%BACKUP_FOLDER%\%BACKUP_FILE%"

echo 'Backup successfully created: %BACKUP_FILE%'