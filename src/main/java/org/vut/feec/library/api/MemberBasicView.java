package org.vut.feec.library.api;
import javafx.beans.property.*;

public class MemberBasicView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty mail = new SimpleStringProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty surname = new SimpleStringProperty();
    private StringProperty membership = new SimpleStringProperty();

    public Long getId() {
        return idProperty().get();
    }

    public void setId(Long id) {
        this.idProperty().setValue(id);
    }

    public String getMail() {
        return mailProperty().get();
    }

    public void setMail(String mail) {
        this.mailProperty().setValue(mail);
    }

    public String getName() { return nameProperty().get();}

    public void setName(String name) {
        this.nameProperty().setValue(name);
    }

    public String getSurname() {
        return surnameProperty().get();
    }

    public void setSurname(String surname) {
        this.surnameProperty().setValue(surname);
    }

    public String getMembership() { return membershipProperty().get();}

    public void setMembership(String membership) {
        this.membershipProperty().setValue(membership);
    }


    public LongProperty idProperty() {
        return id;
    }

    public StringProperty mailProperty() {
        return mail;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty surnameProperty() {
        return surname;
    }

    public StringProperty membershipProperty() {
        return membership;
    }

}
