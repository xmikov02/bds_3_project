package org.vut.feec.library.api;

public class MemberEditView {
    private Long id;
    private String mail;
    private String name;
    private String surname;
    private String membership;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "MemberEditView{" +
                "email='" + mail + '\'' +
                ", firstName='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", active_membership='" + membership + '\'' +
                '}';
    }

}
