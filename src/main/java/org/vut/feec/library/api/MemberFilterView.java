package org.vut.feec.library.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MemberFilterView {
    private final LongProperty id = new SimpleLongProperty();
    private final StringProperty mail = new SimpleStringProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty surname = new SimpleStringProperty();
    private final StringProperty membership = new SimpleStringProperty();

    public void setId(long id) {
        this.id.set(id);
    }

    public long getId() {
        return idProperty().get();
    }


    public void setMail(String mail) {
        this.mail.set(mail);
    }

    public String getMail() {
        return mailProperty().get();
    }


    public void setName(String name) {
        this.name.set(name);
    }

    public String getName() {
        return nameProperty().get();
    }


    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getSurname() {
        return surnameProperty().get();
    }


    public void setMembership(String membership) {
        this.membership.set(membership);
    }

    public String getMembership() {
        return membershipProperty().get();
    }


    public LongProperty idProperty() {
        return id;
    }

    public StringProperty mailProperty() {
        return mail;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty surnameProperty() {
        return surname;
    }

    public StringProperty membershipProperty() {
        return membership;
    }
}
