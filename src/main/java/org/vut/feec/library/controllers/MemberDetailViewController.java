package org.vut.feec.library.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import org.vut.feec.library.api.MemberDetailView;


public class MemberDetailViewController {
    //private static final Logger logger = LoggerFactory.getLogger(MemberDetailViewController.class);
    private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
    @FXML
    private TextField idTextField;

    @FXML
    private TextField mailTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField surnameTextField;

    @FXML
    private TextField membershipTextField;

    @FXML
    private TextField countryTextField;

    @FXML
    private TextField cityTextField;

    @FXML
    private TextField houseNumberTextField;

    @FXML
    private TextField streetTextField;

    @FXML
    private TextField titleTextField;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public MemberDetailViewController() {
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        mailTextField.setEditable(false);
        nameTextField.setEditable(false);
        surnameTextField.setEditable(false);
        membershipTextField.setEditable(false);
        countryTextField.setEditable(false);
        cityTextField.setEditable(false);
        houseNumberTextField.setEditable(false);
        streetTextField.setEditable(false);
        titleTextField.setEditable(false);

        loadMemberData();

        logger.info("MemberDetailViewController initialized");
    }

    private void loadMemberData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof MemberDetailView) {
            MemberDetailView memberBasicView = (MemberDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(memberBasicView.getId()));
            mailTextField.setText(memberBasicView.getMail());
            nameTextField.setText(memberBasicView.getName());
            surnameTextField.setText(memberBasicView.getSurname());
            membershipTextField.setText(memberBasicView.getMembership());
            countryTextField.setText(memberBasicView.getCountry());
            cityTextField.setText(memberBasicView.getCity());
            houseNumberTextField.setText(memberBasicView.getHouseNumber());
            streetTextField.setText(memberBasicView.getStreet());
            titleTextField.setText(memberBasicView.getTitle());
        }
    }
}
