package org.vut.feec.library.controllers;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.vut.feec.library.api.MemberCreateView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class MemberCreateController {

    private static final Logger logger = LoggerFactory.getLogger(MemberCreateController.class);

    @FXML
    public Button newMemberCreateMember;

    @FXML
    private TextField newMemberMail;

    @FXML
    private TextField newMemberName;

    @FXML
    private TextField newMemberSurname;

    @FXML
    private TextField newMemberMembership;

    @FXML
    private TextField newMemberPwd;

    private MemberService memberService;
    private MemberRepository memberRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newMemberMail, Validator.createEmptyValidator("The mail must not be empty."));
        validation.registerValidator(newMemberName, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(newMemberSurname, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(newMemberMembership, Validator.createEmptyValidator("The membership must not be empty."));
        validation.registerValidator(newMemberPwd, Validator.createEmptyValidator("The password must not be empty."));

        newMemberCreateMember.disableProperty().bind(validation.invalidProperty());

        logger.info("MemberCreateController initialized");
    }

    @FXML
    void handleCreateNewMember(ActionEvent event) {
        String mail = newMemberMail.getText();
        String name = newMemberName.getText();
        String surname = newMemberSurname.getText();
        String membership = newMemberMembership.getText();
        String password = newMemberPwd.getText();

        MemberCreateView memberCreateView = new MemberCreateView();
        memberCreateView.setPwd(password.toCharArray());
        memberCreateView.setMail(mail);
        memberCreateView.setName(name);
        memberCreateView.setSurname(surname);
        memberCreateView.setMembership(membership);

        memberService.createMember(memberCreateView);

        memberCreatedConfirmationDialog();
    }

    private void memberCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Member Created Confirmation");
        alert.setHeaderText("Your member was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
