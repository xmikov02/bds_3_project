package org.vut.feec.library.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.vut.feec.library.api.InjectionView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;

import java.util.List;

public class InjectionController {

    @FXML
    private TableColumn<InjectionView, Long> id;

    @FXML
    private TableColumn<InjectionView, String> name;

    @FXML
    private TableColumn<InjectionView, String> surname;

    @FXML
    private TableView<InjectionView> systemMemberTableView;
    @FXML
    private TextField inputField;

    private MemberService memberService;
    private MemberRepository memberRepository;

    public Stage stage;
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        id.setCellValueFactory(new PropertyValueFactory<InjectionView, Long>("id"));
        name.setCellValueFactory(new PropertyValueFactory<InjectionView, String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<InjectionView, String>("surname"));
    }
    private ObservableList<InjectionView> initializeMemberData() {

        String input = inputField.getText();
        List<InjectionView> members = memberService.getInjectionView(input);
        return FXCollections.observableArrayList(members);
    }
    public void handleConfirmButton(ActionEvent actionEvent){
        ObservableList<InjectionView> observableMemberList = initializeMemberData();
        systemMemberTableView.setItems(observableMemberList);
    }

}
