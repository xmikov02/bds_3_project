package org.vut.feec.library.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.vut.feec.library.api.MemberBasicView;
import org.vut.feec.library.api.MemberEditView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class MemberEditController {
    private static final Logger logger = LoggerFactory.getLogger(MemberEditController.class);

    @FXML
    public Button editMemberButton;
    @FXML
    public TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField surnameTextField;
    @FXML
    private TextField mailTextField;
    @FXML
    private TextField membershipTextField;

    private MemberService memberService;
    private MemberRepository memberRepository;
    private ValidationSupport validation;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(surnameTextField, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(mailTextField, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(membershipTextField, Validator.createEmptyValidator("The nickname must not be empty."));

        editMemberButton.disableProperty().bind(validation.invalidProperty());

        loadMemberData();

        logger.info("MemberEditController initialized");
    }

    /**
     * Load passed data from Persons controller. Check this tutorial explaining how to pass the data between controllers: https://dev.to/devtony101/javafx-3-ways-of-passing-information-between-scenes-1bm8
     */
    private void loadMemberData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof MemberBasicView) {
            MemberBasicView memberBasicView = (MemberBasicView) stage.getUserData();
            idTextField.setText(String.valueOf(memberBasicView.getId()));
            nameTextField.setText(memberBasicView.getName());
            surnameTextField.setText(memberBasicView.getSurname());
            mailTextField.setText(memberBasicView.getMail());
            membershipTextField.setText(String.valueOf(memberBasicView.getMembership()));

        }
    }

    @FXML
    public void handleEditMemberButton(ActionEvent event) {
        Long id = Long.valueOf(idTextField.getText());
        String name = nameTextField.getText();
        String surname = surnameTextField.getText();
        String mail = mailTextField.getText();
        String membership = membershipTextField.getText();

        MemberEditView memberEditView = new MemberEditView();
        memberEditView.setId(id);
        memberEditView.setName(name);
        memberEditView.setSurname(surname);
        memberEditView.setMail(mail);
        memberEditView.setMembership(membership);

        memberService.editMember(memberEditView);

        memberEditedConfirmationDialog();
    }

    private void memberEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Member Edited Confirmation");
        alert.setHeaderText("Your member was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
