package org.vut.feec.library.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.List;
import org.vut.feec.library.App;
import org.vut.feec.library.api.MemberBasicView;
import org.vut.feec.library.api.MemberDetailView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.exceptions.ExceptionHandler;
import org.vut.feec.library.services.MemberService;

public class MemberController {

    private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

    @FXML
    public Button addMemberButton;
    @FXML
    public Button refreshButton;
    @FXML
    public Button filterButton;
    @FXML
    public TextField searchBar;
    @FXML
    private TableColumn<MemberBasicView, Long> memberId;

    @FXML
    private TableColumn<MemberBasicView, String> memberMail;
    @FXML
    private TableColumn<MemberBasicView, String> memberName;
    @FXML
    private TableColumn<MemberBasicView, String> memberSurname;
    @FXML
    private TableColumn<MemberBasicView, String> memberMembership;
    @FXML
    private TableView<MemberBasicView> systemMemberTableView;
    @FXML
    public MenuItem exitMenuItem;

    private MemberService memberService;
    private MemberRepository memberRepository;

    public MemberController() {
    }

    @FXML
    private void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        memberId.setCellValueFactory(new PropertyValueFactory<MemberBasicView, Long>("id"));
        memberMail.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("mail"));
        memberName.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("name"));
        memberSurname.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("surname"));
        memberMembership.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("membership"));

        ObservableList<MemberBasicView> observableMemberList = initializeMemberData();
        systemMemberTableView.setItems(observableMemberList);
        systemMemberTableView.getSortOrder().add(memberId);

        initializeTableViewSelection();
        logger.info("MemberController initialized");
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit member");
        MenuItem detailedView = new MenuItem("Detailed member view");
        MenuItem delete = new MenuItem ("Delete member");
        edit.setOnAction((ActionEvent event) -> {
            MemberBasicView memberView = systemMemberTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/MemberEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(memberView);
                stage.setTitle("Edit Member");

                MemberEditController controller = new MemberEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        detailedView.setOnAction((ActionEvent event) -> {
            MemberBasicView memberView = systemMemberTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/MemberDetailView.fxml"));
                Stage stage = new Stage();

                Long memberId = memberView.getId();
                MemberDetailView memberDetailView = memberService.getMemberDetailView(memberId);

                stage.setUserData(memberDetailView);
                stage.setTitle("Member Detailed View");

                MemberDetailViewController controller = new MemberDetailViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        delete.setOnAction((ActionEvent event)-> {
            MemberBasicView memberView = systemMemberTableView.getSelectionModel().getSelectedItem();
            Long memberId = memberView.getId();
            memberRepository.deleteMember(memberId);
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(detailedView);
        menu.getItems().add(delete);
        systemMemberTableView.setContextMenu(menu);
    }

    private ObservableList<MemberBasicView> initializeMemberData() {
        List<MemberBasicView> member = memberService.getMemberBasicView();
        return FXCollections.observableArrayList(member);
    }

    public void handleExitMenuItem(ActionEvent event) {
        System.exit(0);
    }

    public void handleAddMemberButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/MemberCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("Create library member");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<MemberBasicView> observableMemberList = initializeMemberData();
        systemMemberTableView.setItems(observableMemberList);
        systemMemberTableView.refresh();
        systemMemberTableView.sort();
    }

    public void handleFilterButton(ActionEvent actionEvent){
        try {
            String data = searchBar.getText();
            System.out.println("handler" + data);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/Filter.fxml"));
            Stage stage = new Stage();

            MemberFilterController memberFilterController = new MemberFilterController();
            stage.setUserData(data);
            memberFilterController.setStage(stage);
            fxmlLoader.setController(memberFilterController);
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setTitle("Filtered view");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleInjectionButton(ActionEvent actionEvent){
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/Injection.fxml"));
            Stage stage = new Stage();
            InjectionController injectionController = new InjectionController();

            injectionController.setStage(stage);
            fxmlLoader.setController(injectionController);
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setTitle("SQL Injection training");
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }
}
