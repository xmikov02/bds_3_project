package org.vut.feec.library.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.vut.feec.library.api.MemberFilterView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;
import java.util.List;

public class MemberFilterController {
    @FXML
    public TableColumn<MemberFilterView, Long> memberId;

    @FXML
    public TableColumn<MemberFilterView, String> mail;
    @FXML
    public TableColumn<MemberFilterView, String> name;
    @FXML
    public TableColumn<MemberFilterView, String> surname;
    @FXML
    public TableColumn<MemberFilterView, String> membership;
    @FXML
    public TableView<MemberFilterView> systemMemberTableView;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private MemberService memberService;
    private MemberRepository memberRepository;
    @FXML
    private void initialize(){
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        memberId.setCellValueFactory(new PropertyValueFactory<MemberFilterView, Long>("id"));
        mail.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("mail"));
        name.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("surname"));
        membership.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("membership"));

        ObservableList<MemberFilterView> observableMemberList = initializeMemberData();
        systemMemberTableView.setItems(observableMemberList);
    }
    private ObservableList<MemberFilterView> initializeMemberData() {
        String data = (String) stage.getUserData();
        System.out.println("controller " + data);

        List<MemberFilterView> members = memberService.getMemberFilterView(data);
        return FXCollections.observableArrayList(members);

    }
}