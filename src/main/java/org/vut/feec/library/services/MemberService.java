package org.vut.feec.library.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.vut.feec.library.api.*;
import org.vut.feec.library.data.MemberRepository;

import java.util.List;

public class MemberService {
    private MemberRepository memberRepository;

    public MemberService(MemberRepository memberRepository) {this.memberRepository = memberRepository;}

    public MemberDetailView getMemberDetailView(Long id) {
        return memberRepository.findMemberDetailedView(id);
    }

    public List<MemberBasicView> getMemberBasicView() {
        return memberRepository.getMemberBasicView();
    }

    public void editMember(MemberEditView memberEditView) {
        memberRepository.editMember(memberEditView);
    }

    public void createMember(MemberCreateView memberCreateView) { memberRepository.createMember(memberCreateView);}

    public List<MemberFilterView> getMemberFilterView(String data){ return memberRepository.getMemberFilterView(data);}

    public List<InjectionView> getInjectionView(String input){
        return memberRepository.getInjectionView(input);
    }

    private char[] hashPassword(char[] password) {
        return BCrypt.withDefaults().hashToChar(12, password);
    }
}