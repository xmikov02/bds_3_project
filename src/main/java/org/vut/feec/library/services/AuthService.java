package org.vut.feec.library.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.vut.feec.library.api.MemberAuthView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.exceptions.ResourceNotFoundException;

public class AuthService {

    private MemberRepository memberRepository;

    public AuthService(MemberRepository memberRepository){
        this.memberRepository = memberRepository;
    }

    private MemberAuthView findMemberByMail(String mail) {
        return memberRepository.findMemberByMail(mail);
    }

    public boolean authenticate(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        MemberAuthView memberAuthView = findMemberByMail(username);
        if (memberAuthView == null) {
            throw new ResourceNotFoundException("Provided username is not found.");
        }
            BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), memberAuthView.getPassword());
            return result.verified;}
}
//sign up as role db_user:
//db_user
//ilovebds