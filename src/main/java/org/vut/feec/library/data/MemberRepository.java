package org.vut.feec.library.data;

import org.vut.feec.library.api.MemberAuthView;
import org.vut.feec.library.config.DataSourceConfig;
import org.vut.feec.library.exceptions.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.vut.feec.library.api.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MemberRepository {

    public MemberAuthView findMemberByMail(String mail) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT mail, pwd" +
                             " FROM public.member m" +
                             " WHERE m.mail = ?")
        ) {
            preparedStatement.setString(1, mail);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    MemberAuthView member = new MemberAuthView();
                    member.setMail(resultSet.getString("mail"));
                    member.setPassword(resultSet.getString("pwd"));
                    return member;
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find member by mail failed.", e);
        }
        return null;
    }

    public MemberDetailView findMemberDetailedView(Long memberId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT m.member_id , m.mail, m.name, m.surname, m.active_membership, a.country, a.city, a.street, a.house_number, bk.title" +
                             " FROM public.member m" +
                             " LEFT JOIN public.member_has_address h ON h.member_id = m.member_id" +
                             " LEFT JOIN public.address a ON h.address_id = a.address_id" +
                             " LEFT JOIN public.member_can_borrow b ON b.member_id = m.member_id" +
                             " LEFT JOIN public.book bk ON bk.book_id = b.book_id" +
                             " WHERE m.member_id = ?")
        ) {
            preparedStatement.setLong(1, memberId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToMemberDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find member by ID with addresses failed.", e);
        }
        return null;
    }

    public List<MemberBasicView> getMemberBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT m.member_id, m.mail, m.name, m.surname, m.active_membership" +
                             " FROM public.member m");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<MemberBasicView> memberBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                memberBasicViews.add(mapToMemberBasicView(resultSet));
            }
            return memberBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Member basic view could not be loaded.", e);
        }
    }
    public void deleteMember(Long id) {
        String deleteMemberSQL =  "DELETE FROM public.member WHERE member_id = ?";
        System.out.println(deleteMemberSQL);
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement prep = connection.prepareStatement(deleteMemberSQL)){

            prep.setLong(1, id);
            prep.executeUpdate();

        }
        catch (SQLException e) {
            System.out.println("Member removal failed");
        }
    }
    public void createMember(MemberCreateView memberCreateView) {
        String insertMemberSQL = "INSERT INTO public.member (mail, name, surname, active_membership, pwd) VALUES (?,?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertMemberSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setString(1, memberCreateView.getMail());
            preparedStatement.setString(2, memberCreateView.getName());
            preparedStatement.setString(3, memberCreateView.getSurname());
            preparedStatement.setString(4, String.valueOf(memberCreateView.getMembership()));
            preparedStatement.setString(5, String.valueOf(memberCreateView.getPwd()));


            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating member failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating member failed operation on the database failed.");
        }
    }

    public void editMember(MemberEditView memberEditView) {
        String insertPersonSQL = "UPDATE public.member m SET mail = ?, name = ?, surname = ?, active_membership = ? " +
                                 "WHERE m.member_id = ?";
        String checkIfExists = "SELECT mail FROM public.member m WHERE m.member_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setString(1, memberEditView.getMail());
            preparedStatement.setString(2, memberEditView.getName());
            preparedStatement.setString(3, memberEditView.getSurname());
            preparedStatement.setString(4, memberEditView.getMembership());
            preparedStatement.setLong(5, memberEditView.getId());

            try {
                // TODO set connection autocommit to false
                /* HERE */
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, memberEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This member for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating member failed, no rows affected.");
                }
                // TODO commit the transaction (both queries were performed)
                /* HERE */
            } catch (SQLException e) {
                // TODO rollback the transaction if something wrong occurs
                /* HERE */
            } finally {
                // TODO set connection autocommit back to true
                /* HERE */
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating member failed operation on the database failed.");
        }
    }

    public List<InjectionView> getInjectionView(String input){
        String query =  "SELECT member_id,name,surname " +
                "FROM public.member " +
                "WHERE member_id = '" +
                input + "'";
        //1'; DROP TABLE public.drop_me;--
        //5' OR 1=1 --
        /*It is necessary to learn and use PreparedStatements because they are helpful in protecting from SQL Injection attacks.
        Plus, its almost requirement to use PreparedStatement for queries with user input.
        Sometimes it is more convenient to use a PreparedStatement object for sending SQL statements to the database.*/
        try (Connection connection = DataSourceConfig.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            List<InjectionView> injectionViews = new ArrayList<>();
            while (resultSet.next()) {
                injectionViews.add(mapToInjectionView(resultSet));
            }
            return injectionViews;
        } catch (SQLException e) {
            throw new DataAccessException("Failed.", e);
        }
    }

    public List<MemberFilterView> getMemberFilterView(String data){
        System.out.println(data);
        String mail = '%' + data + '%';
        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT member_id, mail, name, surname, active_membership" +
                             " FROM public.member" +
                             " WHERE mail like ?"
             )
        ) {
            preparedStatement.setString(1, mail);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<MemberFilterView> memberFilterViews = new ArrayList<>();
            while (resultSet.next()) {
                memberFilterViews.add(mapToMemberFilterView(resultSet));
            }
            return memberFilterViews;
        }

        catch (SQLException e) {
            throw new DataAccessException("Member filter view could not be loaded.", e);
        }
    }

    private MemberFilterView mapToMemberFilterView(ResultSet rs) throws SQLException{
        MemberFilterView memberFilterView = new MemberFilterView();
        memberFilterView.setId(rs.getLong("member_id"));
        memberFilterView.setMail(rs.getString("mail"));
        memberFilterView.setName(rs.getString("name"));
        memberFilterView.setSurname(rs.getString("surname"));
        memberFilterView.setMembership(rs.getString("active_membership"));
        return memberFilterView;
    }

    private MemberBasicView mapToMemberBasicView(ResultSet rs) throws SQLException {
        MemberBasicView memberBasicView = new MemberBasicView();
        memberBasicView.setId(rs.getLong("member_id"));
        memberBasicView.setMail(rs.getString("mail"));
        memberBasicView.setName(rs.getString("name"));
        memberBasicView.setSurname(rs.getString("surname"));
        memberBasicView.setMembership(rs.getString("active_membership"));
        return memberBasicView;
    }

    private MemberDetailView mapToMemberDetailView(ResultSet rs) throws SQLException {
        MemberDetailView memberDetailView = new MemberDetailView();
        memberDetailView.setId(rs.getLong("member_id"));
        memberDetailView.setMail(rs.getString("mail"));
        memberDetailView.setName(rs.getString("name"));
        memberDetailView.setSurname(rs.getString("surname"));
        memberDetailView.setMembership(rs.getString("active_membership"));
        memberDetailView.setCountry(rs.getString("country"));
        memberDetailView.setCity(rs.getString("city"));
        memberDetailView.setStreet(rs.getString("street"));
        memberDetailView.setHouseNumber(rs.getString("house_number"));
        memberDetailView.setTitle(rs.getString("title"));
        return memberDetailView;
    }

    private InjectionView mapToInjectionView(ResultSet rs ) throws  SQLException{
        InjectionView injectionView = new InjectionView();
        injectionView.setId(rs.getLong("member_id"));
        injectionView.setName(rs.getString("name"));
        injectionView.setSurname(rs.getString("surname"));
        return injectionView;
    }
}
